import { useState, useEffect } from "react";

const API_BASE = "http://localhost:3001";

function App() {

  const [students, setStudents] = useState([]);
  const [popUpActive, setPopupActive] = useState(false);
  const [newStudent, setNewStudent] = useState("");

  useEffect(() => {
    GetStudents();
  }, [students]);

  const GetStudents = () =>{
    fetch(API_BASE + "/students")
      .then(res => res.json())
      .then(data=> setStudents(data))
      .catch(err => console.error("error: " + err));
  }

  const viewStudent = async id => {
    const data = await fetch(API_BASE + "/student/" + id)
      .then(res => res.json())
      .catch(err => console.error("error: " + err));

      setStudents(students => students.filter(student=> student._id !== data._id));
  }

  const addStudent = async () => {
    console.log(newStudent);
    const data = await fetch(API_BASE + "/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({text: newStudent})
    })
      .then(res => res.json())
      .catch(err => console.error("error: " + err));

    console.log(data);
    setStudents([...students, data]);
    setPopupActive(false);
    newStudent("")
  }

  const updateStudent = async id => {
    console.log(newStudent);
    const data = await fetch(API_BASE + "/student/" + id +"/edit", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({text: newStudent})
    })
      .then(res => res.json())
      .catch(err => console.error("error: " + err));

    console.log(data);
    setStudents([...students, data]);
    setPopupActive(false);
    newStudent("")
  }


  return (
    
    <div className="App">
      <h1>Welcome MIT Students Portal</h1>

      <br/>
      <button onClick={() => setPopupActive(true)} >Add (+)</button>
      <br/><br/>
      
      <table className="">
        <thead>
          <tr>
            <th>Full Name</th>
            <th>Email Address</th>
            <th>Contact</th>
            <th>Emergency Contact</th>
            <th>T-shirt Size</th>
            <th>Address</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>

          {students.map((student) => (
            <tr key={student.id}>
              <td>{student.fullName}</td>
              <td>{student.emailAddress}</td>
              <td>{student.contact}</td>
              <td>{student.emergencyContact}</td>
              <td>{student.tshirtSize}</td>
              <td>{student.address}</td>
              <td></td>
            </tr>
          ))}
 
        </tbody>
      </table>

      <hr></hr>
      
      {
        popUpActive ? (
          <div className="popup">
            <div className="closePopup" onClick={() => setPopupActive(false)}></div>
            <div className="content">
              <h3>Add Student</h3>

              <label>
                Name:
                <input
                  type="text"
                  value={newStudent.fullName}
                  onChange={e => setNewStudent(e.target.value)}
                />
              </label><br/>
              <label>
                emailAddress:
                <input
                  type="text"
                  value={newStudent.emailAddress}
                  onChange={e => setNewStudent(e.target.value)}
                />
              </label><br/>
              <label>
                contact:
                <input
                  type="text"
                  value={newStudent.contact}
                  onChange={e => setNewStudent(e.target.value)}
                />
              </label><br/>
              <label>
                emergencyContact:
                <input
                  type="text"
                  value={newStudent.emergencyContact}
                  onChange={e => setNewStudent(e.target.value)}
                />
              </label><br/>
              <label>
                tshirtSize:
                <input
                  type="text"
                  value={newStudent.tshirtSize}
                  onChange={e => setNewStudent(e.target.value)}
                />
              </label><br/>
                <label>address:</label>
                <input
                  type="text"
                  value={newStudent.address}
                  onChange={e => setNewStudent(e.target.value)}
                /><br/><br/>


              <button onSubmit={addStudent}>Create Student</button>
              <button onSubmit={() => setPopupActive(false)}>Cancel</button>
            </div>
          </div>
        ) : ''
      }
    </div>
  );
}

export default App;
