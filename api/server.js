const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();


app.use(express.json());
app.use(cors());

mongoose.connect("mongodb://localhost:27017/student-db", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log("Connected to DB"))
    .catch(console.error);

const Student = require('./models/student');


app.get('/students', async (req, res) => {
    const students = await Student.find();

    res.json(students);
});

app.post('/register', async (req, res) => {
    const student = new Student({
        fullName: req.body.fullName,
        emailAddress: req.body.emailAddress,
        contact: req.body.contact,
        emergencyContact: req.body.emergencyContact,
        tshirtSize: req.body.tshirtSize,
        address: req.body.address
    });

    student.save();
    res.json(student);
});

app.get('/student/:id', async (req, res) => {

    const student = await Student.findById(req.params.id);

    res.json(student);
});

app.put('/student/:id/edit', async (req, res) => {
    const student = await Student.findById(req.params.id);

    student.fullName = req.body.fullName;
    student.emailAddress = req.body.emailAddress;
    student.contact = req.body.contact;
    student.emergencyContact = req.body.emergencyContact;
    student.tshirtSize = req.body.tshirtSize;
    student.address = req.body.address;
    student.fullName = req.body.fullName;

    student.save();
    res.json(student);
});


app.listen(3001, () => console.log("Server started on port 3001"));