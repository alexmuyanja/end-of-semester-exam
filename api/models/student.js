const mongoose = require('mongoose');

// Create a new schema
const studentSchema = mongoose.Schema({
  fullName: {
    type: String,
    required: true
  },
  emailAddress: {
    type: String,
    required: true,
    unique: true
  },
  contact: {
    type: String,
    required: true
  },
  emergencyContact: {
    type: String,
    required: true
  },
  tshirtSize: {
    type: String
  },
  address: {
    type: String
  }
});

// Create the model based on the schema
const Student = mongoose.model('Student', studentSchema);

// Export the model
module.exports = Student;
